package com.estee.prototype.prototypesgit.controller;

import com.estee.prototype.prototypesgit.service.GitService;
import java.io.IOException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/git")
public class GitController {
    private final GitService gitService;

    @Autowired
    public GitController(GitService gitService) {
        this.gitService = gitService;
    }

    @PostMapping("/setup")
    public ResponseEntity setup() throws GitAPIException {
        gitService.cloneWithSSH();
        return new ResponseEntity<>("setup", HttpStatus.OK);
    }

    @PostMapping("/setup-http")
    public ResponseEntity setupWithHttp() throws GitAPIException {
        //add username and password in GitService if you want to use this
        gitService.cloneWithUsernamePassword();
        return new ResponseEntity<>("setup", HttpStatus.OK);
    }

    @GetMapping("/remote")
    public ResponseEntity getBranches() throws GitAPIException, IOException {
        GitService.initGIT();
        return new ResponseEntity<>(gitService.getAllRemoteBranches(), HttpStatus.OK);
    }

    @GetMapping("/local")
    public ResponseEntity getBranchesLOCALLY() throws GitAPIException, IOException {
        GitService.initGIT();
        return new ResponseEntity<>(gitService.getAllLocalBranches(), HttpStatus.OK);
    }

    @PostMapping("/checkout/{name}")
    public ResponseEntity checkout(@PathVariable("name") String name) throws GitAPIException, IOException {
        GitService.initGIT();
        gitService.checkout(name);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity createBranch(@RequestParam("name") String branchName, @RequestParam(value = "parent", required = false) String parent) throws GitAPIException, IOException {
        GitService.initGIT();
        gitService.createBranch(branchName, parent);
        return new ResponseEntity<>("createBranch", HttpStatus.OK);
    }

    @PatchMapping
    public ResponseEntity commitAndPush() throws IOException, GitAPIException {
        GitService.initGIT();
        gitService.commitAndPush("from java", "commit from controller");
        return new ResponseEntity<>("commitAndPush", HttpStatus.OK);
    }
}

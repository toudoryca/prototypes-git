package com.estee.prototype.prototypesgit.service;

import com.jcraft.jsch.Session;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class GitService {
    private static final String URL_PATH_SSH = "ssh";
    private static final String URL_PATH_HTTP = "http";
    private static final String DIRECTORY_PATH = "..\\test-repo\\";

    //Add username and password if you want to use http url for git
    private static final String USERNAME = "";
    private static final String PASSWORD = "";
    private static final UsernamePasswordCredentialsProvider CREDENTIALS_PROVIDER = new UsernamePasswordCredentialsProvider(USERNAME, PASSWORD);
    private static Git GIT_BUN = null;

    /**
     * Opens the local repository
     */
    public static void initGIT() throws IOException {
        if (GIT_BUN == null)
            GIT_BUN = Git.open(new File(DIRECTORY_PATH));
    }

    /**
     * Clones the remote repository using SSH
     */
    public void cloneWithSSH() throws GitAPIException {
        SshSessionFactory sshSessionFactory = new JschConfigSessionFactory() {
            @Override
            protected void configure(OpenSshConfig.Host host, Session session) {
                // do nothing
            }
        };
        CloneCommand cloneCommand = Git.cloneRepository();
        cloneCommand.setDirectory(new File(DIRECTORY_PATH));
        cloneCommand.setURI(URL_PATH_SSH);
        cloneCommand.setTransportConfigCallback(transport -> {
            SshTransport sshTransport = (SshTransport) transport;
            sshTransport.setSshSessionFactory(sshSessionFactory);
        });
        cloneCommand.call();
    }

    /**
     * Get all remote branches
     */
    public List<String> getAllRemoteBranches() throws GitAPIException {
        System.out.println("remote branches");
        Collection<Ref> refCollection = Git.lsRemoteRepository()
                .setHeads(true)
                .setRemote(URL_PATH_SSH)
                .setCredentialsProvider(new UsernamePasswordCredentialsProvider(USERNAME, PASSWORD))
                .call();
        System.out.println("#=" + refCollection.size());
        refCollection.forEach(ref -> System.out.println("RR - Branch: " + ref.getName()));
        return refCollection.stream().map(Ref::getName).sorted().collect(Collectors.toList());
    }

    /**
     * Gets all the local branches
     */
    public List<String> getAllLocalBranches() throws GitAPIException {
        System.out.println("LOCAL branches");
        List<Ref> refCollection = GIT_BUN.branchList().call();
        System.out.println("#=" + refCollection.size());
        refCollection.forEach(ref -> System.out.println("LL - Branch: " + ref.getName()));
        return refCollection.stream().map(Ref::getName).sorted().collect(Collectors.toList());
    }

    /**
     * Clone the remote repository using HTTP
     */
    public void cloneWithUsernamePassword() throws GitAPIException {
        //add username and password if you want to use this
        Git.cloneRepository()
                .setURI(URL_PATH_HTTP)
                .setCredentialsProvider(CREDENTIALS_PROVIDER)
                .setDirectory(new File(DIRECTORY_PATH))
                .setBare(false)
                .call();
    }

    /**
     * Checkout to the specified branch name
     */
    public void checkout(String branchName) throws GitAPIException {
        fetch();
        GIT_BUN.checkout()
                .setName(branchName)
                .setStartPoint("origin/" + branchName)
                .setCreateBranch(!localBranchExists(branchName))
                .call();
    }

    /**
     * Checks if the branch name exists locally
     */
    private static boolean localBranchExists(String branchName) {
        try {
            return GIT_BUN.branchList().call().stream()
                    .map(Ref::getName)
                    .map(s -> s.replaceFirst("^refs/heads/", ""))
                    .anyMatch(b -> b.equals(branchName));
        } catch (GitAPIException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates a branch from master or from a specified branch
     */
    public void createBranch(String branchName, String parent) throws GitAPIException, IOException {
        fetch();
        GIT_BUN.checkout()
                .setName(branchName)
                .setStartPoint("origin/" + (StringUtils.isEmpty(parent) ? "master" : parent))
                .setCreateBranch(true)
                .call();
        commitAndPush(String.join("", "from ", branchName, " branch"), String.join(" ", "Create branch", branchName, "from parent", parent));
    }

    /**
     * Fetch branches from remote repository
     */
    private void fetch() throws GitAPIException {
        System.out.println("try call fetch");
        GIT_BUN.fetch().setRemoveDeletedRefs(true).call();

        System.out.println("fetch successful ");
    }

    /**
     * Commit and push
     */
    public void commitAndPush(String fileContent, String commitMessage) throws GitAPIException, IOException {
        Files.write(Paths.get(DIRECTORY_PATH + "master.txt"), fileContent.getBytes());
        GIT_BUN.add().addFilepattern(".").call();
        GIT_BUN.commit().setMessage(commitMessage).call();
        GIT_BUN.push().call();
    }
}

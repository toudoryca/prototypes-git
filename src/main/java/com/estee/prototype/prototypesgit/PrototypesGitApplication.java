package com.estee.prototype.prototypesgit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrototypesGitApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrototypesGitApplication.class, args);
	}

}

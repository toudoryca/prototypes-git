
# Java Checkout Service

This repository will demonstrate how work with GIT using java


## Design and implementation ##
The application is written using **spring boot**.
It uses **spring web starter** dependency for the **controllers and endpoints**.

It uses the standard layers of a spring boot application:
* **controllers** - who expose the endpoints
* **service** - for business logic

Functionality implemented:
* connect to ssh
* clone localy the repository
* get all remote branches
* get all local branches
* create branch
* checkout branch
* commit and push 

## Setup ##
* Clone the repository

### Install Dependencies
* Java 10
* Gradle 5.4.1


## Run ##

./gradlew bootRun
